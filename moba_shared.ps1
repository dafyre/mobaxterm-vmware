<#

Generate Shared session file for MobaXTerm.

Date        Updated By  Details
8-22-2022      DaFyre      Initial Commit.

#>

Param(
    [Parameter(Mandatory = $true)] [string[]] $vcServers,
    [Parameter(Mandatory = $false)] [string] $cacheFile="",
    [Parameter(Mandatory = $false)] [bool] $refreshCache=$false,
    [Parameter(Mandatory = $false)] [string] $mobaFile="shared_sessions.mxtsession",
    [Parameter(Mandatory = $false)] [string] $credFile="mobacred.cred",
    [Parameter(Mandatory = $false)] [string] $credKey="$credFile.key",
    [Parameter(Mandatory = $false)] [string] $winCredName="[My_RDP_Credential]",
    [Parameter(Mandatory = $false)] [string] $sshCredName="[My_SSH_Credential]"

)  
#>

$hasVI=get-command connect-viserver* -ErrorAction SilentlyContinue

if ([string]::IsNullOrEmpty($hasVI) -eq $true) {
    try {
        import-module VMware.PowerCLI
    }
    catch {
        write-host -foregroundcolor "Yellow" "You MUST have the VMware PowerShell Modules installed for this to work."
        write-host -foregroundcolor "yellow" "Open PowerShell as an administrator and then "
        write-host " "
        write-host -foregroundcolor "white" "install-module vmware.powercli"
        write-host " "
        write-host -foregroundcolor "Yellow" "And then try this script again."

        exit -2048;
    }
}

$isFile=test-path $credFile

if ($isFile -eq $false) {

    write-host -foreground Yellow "Please enter your vCenter Admin Credentials..."
    $username = read-host "Username: "
    $password = read-host "Password: " -AsSecureString
    #$VCCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $password

    #Create a 32Byte key
    $keyData=New-Object Byte[] 32

    [Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($keyData)

    set-content -path $credKey $keyData

    #$stCred=Get-Credential

    $myPassSecure=$password

    $myPass=$myPassSecure | convertfrom-securestring -key $keyData


    set-content -path $credFile $username
    add-content -path $credFile $myPass

    write-host "Credential Stored in $credFile"
    write-host "Key Stored in $credKey"

  }




if ([string]::isnullorempty($credFile) -eq $false) {
    write-host "Loading Credentials from $credFile / $credKey "
    $keyData = get-content $credKey
    $credData=get-content $credFile

    $username=$credData[0]
    $password = $credData[1] | convertto-securestring -key $keyData

    $objCred=new-object System.Management.automation.pscredential -ArgumentList $username,$password    

} else {
    write-host -ForegroundColor Red "This script requires credentials to be passed with the credFile and CredKey parameters!"
    exit -1024

}

if ($vcServers.count -lt 1) {
    write-host -ForegroundColor Red "This script requires one or more vCenter Server to be passed with the vcServers Parameter!"
    exit -2048

}

if ([string]::isnullorempty($cacheFile) -eq $false -and $refreshCache -eq $false) {
    $serverList=import-csv $cacheFile
} else {

    foreach ($vcenter in $vcServers) {
        connect-viserver -credential $objCred $vcenter > $null
    }

    #write-host "Loading Virtual Machines..."
    $vmList=get-vm | where { $_.PowerState -eq 'PoweredOn' }

    $mySystems=$vmList | select Name,Guest,GuestId,@{n='IPAddress'; e={(get-vmguest -vm $_).Nics.IPAddress }},@{n='Datacenter'; e={($_ | get-datacenter).name } }, @{n="Cluster"; e= {($_ | get-cluster).name} }
    $ServerList = new-object System.Collections.ArrayList

    $ipv4Match='^(?:(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)\.){3}(?:0?0?\d|0?[1-9]\d|1\d\d|2[0-5][0-5]|2[0-4]\d)$'

    foreach ($vm in $mySystems) {
        $vmName=$vm.Name
        #$folderName="$($vm.datacenter)\$($vm.cluster)"
        $DCName="$($vm.datacenter)"
        $ClusterName="$($vm.cluster)"
        if ($vm.guest.extensiondata.guestfamily -match "windows") { $vmType="RemoteDesktopConnection"}
        if ($vm.guest.extensiondata.guestfamily -match "Linux") {$vmType="SSH"}
        
        if ([string]::IsNullOrEmpty($vmType) -eq $true) {
            if ($vm.guestid -match "windows") { 
                $vmType="Windows"
            } else {
                $vmType="SSH"
            }
        }

        $IPAddress=($vm.guest.extensiondata.net.ipaddress -match  $ipv4Match)[0]
        if ($IPAddress -eq $true) {
            $IPAddress=$vm.guest.extensiondata.net.ipaddress
        }

        $tmpObj=[pscustomobject]@{
            Name = $vmName
            Datacenter=$DCName
            Cluster=$ClusterName
            ComputerName = $IPAddress
            Type = $vmType
        }

        [void] $serverList.add($tmpObj)

    }
}


$serverList = $serverList | where {$_.Computername -ne "False"} | sort -property Datacenter,Cluster,Name

if ([string]::isnullorempty($cacheFile) -eq $false -and $refreshCache -eq $true) {
    $serverList | Export-Csv -path $cacheFile -NoTypeInformation
}


$data=new-object system.collections.arraylist
[void] $data.add("[Bookmarks]")
[void] $data.add("SubRep=")
[void] $data.add("ImgNum=41")
[void] $data.add("")
#$bookmarkList=new-object System.Collections.ArrayList
$bookmarkCount=1

$dcList=$serverList.datacenter | unique
$clusterList=$serverList.Cluster | unique

foreach ($item in $dcList) {
    [void] $data.add("[Bookmarks_$bookmarkCount]")
    [void] $data.add("SubRep=$item")
    [void] $data.add("ImgNum=41")
    [void] $data.add("")

    $bookmarkCount++
}

foreach ($item in $clusterList) {
    $clDC=$serverList|where {$_.cluster -eq $item}|Select-object -first 1
    $clDC=$clDC.datacenter
    $clPath="$clDC\$item"

    [void] $data.add("[Bookmarks_$bookmarkCount]")
    [void] $data.add("SubRep=$clPath")    
    [void] $data.add("ImgNum=41")
    #[void] $data.add("")
    $bookmarkItems=$serverList|where {$_.Cluster -eq $item}
    foreach ($bkItem in $bookmarkItems) {
        if ($bkItem.type -eq "SSH") {
            $bkType="109" #Linux System
            $bkPort=22
            $bkCred=$sshCredName
        } else {
            $bkType="91" #Windows System
            $bkPort=3389
            $bkCred=$winCredName
        }

        if ($bkType -eq "91") { $lineItem="$($bkItem.name)=#$bkType#4%$($bkitem.computername)%$bkPort%$bkCred%0%-1%-1%-1%-1%0%0%-1%%%22%%1%0%%-1%%-1%-1%0%-1#MobaFont%10%0%0%0%15%68,68,68%255,255,255%170,170,170%0%-1%0%%xterm%-1%1%0,0,0%170,170,170%128,0,0%170,0,0%0,128,0%0,170,0%128,128,0%170,170,0%0,0,128%0,0,170%128,0,128%170,0,170%0,128,128%0,170,170%128,128,128%170,170,170%80%24%0%0%-1%<none>%%0#0# #-1" }
        if ($bkType -eq "109") {$lineItem="$($bkitem.name)=#$bkType#0%$($bkitem.computername)%$bkPort%$bkCred%%-1%-1%%%22%%0%0%0%%%-1%0%0%0%%1080%%0%0%1#MobaFont%10%0%0%0%15%68,68,68%255,255,255%170,170,170%0%-1%0%%xterm%-1%1%0,0,0%170,170,170%128,0,0%170,0,0%0,128,0%0,170,0%128,128,0%170,170,0%0,0,128%0,0,170%128,0,128%170,0,170%0,128,128%0,170,170%128,128,128%170,170,170%80%24%0%0%-1%<none>%%0#0# #-1"}
        [void] $data.add($lineItem)
    }
    [void] $data.add("")

    $bookmarkCount++
}

$data | Out-File -Encoding ascii -filepath $mobaFile

