##MobaXterm VMware Shared Sessions Generator

#### Generates SSH and Windows entries in a layout that mirrors your vCenter Configuration

**Example**

`.\moba_shared.ps1 -vcServers "myvc1.mydomain.local","myvc2.mydomain.local" -mobaFile "my_vcenter_sessions.mxtsession" -credFile "mycredential.cred" -wincredName "[My_RDP_Credential]" -sshCredName "[MySSH_Credential]"
`

* Connects to myvc1 and myvc2 using the credentials stored in "mycredential.cred".

* For any Linux systems, it enables SSH and uses the "MySH_Credential"

* For Windows systems, it enables RDP and uses the "My_RDP_Credential'

* The VMs will follow the same Datacenter structure that you have configured in vCenter Server.

* If the credFile and credKey do not exist, then you will be prompted for your credentials to the vCenter Server.  The files will be saved in the current directory, by default, as mobacred.cred, and mobecred.cred.key respectively.

* In MobaXterm, make sure you have Credentials Named as whatever is passed to winCredName (default = [My_RDP_Credential]) and sshCredName (default = [My_SSH_Credential] ).






